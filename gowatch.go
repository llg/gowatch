// The MIT License (MIT)
// Copyright © 2012 Sketchground <http://sketchground.dk>
// Copyright © 2012 GoTamer <http://www.robotamer.com>
// Copyright © 2014 llg
// See included license file for details

// It watches your dev folder for modikied files,
// and if a file changes it restarts the process.
//
//  * `gowatch go run main.go` will run `go run main.go` on the current folder
//
// If a file changes while running `gowatch [program_name]`
// it will kill [program_name] and re-run [program_name] on the folder
//

package main

import (
	"flag"
	"fmt"
	"github.com/howeyc/fsnotify"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"
)

func main() {
	var err error
	flag.Parse()

	args := flag.Args()
	cmd := Play(args)

	pwd, err := os.Getwd()
	check(err)

	watcher, err := fsnotify.NewWatcher()
	check(err)
	defer watcher.Close()
	err = filepath.Walk(pwd, func(path string, info os.FileInfo, err error) error {
		err = watcher.Watch(path)
		return err
	})

	check(err)
	go func() {
		for {
			select {
			case ev := <-watcher.Event:
				if ev.IsModify() {
					if strings.HasSuffix(ev.Name, ".go") {
						fmt.Println("Killing...")
						kill(cmd.Process)
						cmd = Play(args)

					}
				}
			case err := <-watcher.Error:
				fmt.Println("error", err.Error())
			}
		}
	}()
	for {
		select {}
	}
}

func Play(args []string) *exec.Cmd {
	fmt.Println("Play: ", args)
	cmd := exec.Command(args[0], args[1:]...)
	stdout, err := cmd.StdoutPipe()
	check(err)
	go io.Copy(os.Stdout, stdout)
	stderr, err := cmd.StderrPipe()
	check(err)
	go io.Copy(os.Stderr, stderr)
	if err = cmd.Start(); err != nil {
		fmt.Println(err)
	}
	return cmd
}

func sleep(sec time.Duration) {
	time.Sleep(time.Second * sec)
}

func kill(process *os.Process) {
	if runtime.GOOS == "windows" { // Kill Process Tree on windows
		kill := exec.Command("TASKKILL", "/F", "/T", "/PID", strconv.FormatInt(int64(process.Pid), 10))
		kill.Stderr = os.Stderr
		kill.Stdout = os.Stdout
		kill.Start()
		kill.Wait()
	} else {
		process.Kill()
	}
	process.Wait()
}

func check(err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\nFile: %s\nLine: %d\nError Message:%s\n", file, line, err.Error())
		os.Exit(2)
	}
}
