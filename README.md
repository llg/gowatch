llg gowatch
===============

llg gowatch is a fork of bitbucket.org/gotamer/gowatch with some enhancements && simplification

It watches your dev folder for modified files, and if a file changes it restarts 
the process.  

 * `gowatch go run main.go` will run `go run main.go` on the current folder 

If a file changes while running lets say `gowatch [program_name]` it will re-run [program_name]

### Links
 * [Pkg Documentationn](http://go.pkgdoc.org/bitbucket.org/llg/gowatch "Pkg Documentation")
 * [Repository](https://bitbucket.org/llg/gowatch "Repository")
